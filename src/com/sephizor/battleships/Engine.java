package com.sephizor.battleships;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.google.gson.Gson;

public class Engine {
    private final int maxFails = 10;
    private boolean gameOver = false;
    private Map<ShipType, Ship> ships;
    private Map<ShipType, Integer> shipPoints;
    private Map<Integer, ShipType> shipIds;
    private BufferedReader inputReader;
    private GameGrid grid;

    public Engine(boolean debugMode) {
        grid = new GameGrid();
        ships = new HashMap<ShipType, Ship>();
        shipPoints = new HashMap<ShipType, Integer>();
        shipIds = new HashMap<Integer, ShipType>();
        inputReader = new BufferedReader(new InputStreamReader(System.in));

        shipPoints.put(ShipType.AIRCRAFT_CARRIER, 2);
        shipPoints.put(ShipType.BATTLESHIP, 4);
        shipPoints.put(ShipType.SUBMARINE, 6);
        shipPoints.put(ShipType.DESTROYER, 8);
        shipPoints.put(ShipType.PATROL_BOAT, 10);

        shipIds.put(1, ShipType.AIRCRAFT_CARRIER);
        shipIds.put(2, ShipType.BATTLESHIP);
        shipIds.put(3, ShipType.SUBMARINE);
        shipIds.put(4, ShipType.DESTROYER);
        shipIds.put(5, ShipType.PATROL_BOAT);

        ships.put(ShipType.AIRCRAFT_CARRIER, new Ship(5, 1));
        ships.put(ShipType.BATTLESHIP, new Ship(4, 2));
        ships.put(ShipType.SUBMARINE, new Ship(3, 3));
        ships.put(ShipType.DESTROYER, new Ship(2, 4));
        ships.put(ShipType.PATROL_BOAT, new Ship(1, 5));
        grid.populate(ships);

        run(debugMode);

        try {
            inputReader.close();
        }
        catch(IOException e) {
        }
    }

    private void saveGame(int points) {
        grid.saveGame();
        File pointsFile = new File(".savePoints");
        BufferedWriter writer;
        try {
            writer = new BufferedWriter(new FileWriter(pointsFile, false));
            writer.write("" + points);
            writer.close();
        }
        catch(IOException e) {
        }
    }

    private int loadGame() {
        File saveFile = new File(".save");
        File pointsFile = new File(".savepoints");
        int points = 0;
        if(pointsFile.exists()) {
            BufferedReader reader;
            try {
                reader = new BufferedReader(new FileReader(pointsFile));
                points = Integer.parseInt(reader.readLine());
                reader.close();
            }
            catch(IOException e) {
            }
        }
        if(saveFile.exists()) {
            Gson gson = new Gson();
            BufferedReader reader;
            try {
                reader = new BufferedReader(new FileReader(saveFile));
                String line = "";
                StringBuilder fileContent = new StringBuilder();
                while((line = reader.readLine()) != null) {
                    fileContent.append(line);
                }
                reader.close();
                grid = gson.fromJson(fileContent.toString(), GameGrid.class);
                int[][] restoredGrid = grid.getGrid();
                for(int i = 0; i < 10; i++) {
                    for(int j = 0; j < 10; j++) {
                        int cell = restoredGrid[i][j];
                        if(cell >= 1 && cell <= 5) {
                            ships.get(shipIds.get(cell)).setCoord(i, j);
                        }
                        
                    }
                }
            }
            catch(IOException e) {
            }

        }
        else {
            System.err.println("No save data found");
        }
        return points;
    }

    private void run(boolean debugMode) {
        int fails = 0;
        int hits = 0;
        int maxHits = ships.size();
        int totalPoints = 0;
        int[][] currentGrid;
        boolean win = false;

        while(!gameOver) {
            if(fails == maxFails) {
                gameOver = true;
                continue;
            }
            currentGrid = grid.getGrid();
            if(debugMode) {
                for(int i = 0; i < 10; i++) {
                    for(int j = 0; j < 10; j++) {
                        System.out.print(currentGrid[j][i]);
                    }
                    System.out.println();
                }
            }
            System.out.print("Press s to save, l to load, x to exit. Press enter to play: ");
            String choice = "";
            try {
                choice = inputReader.readLine();
            }
            catch(IOException e1) {
            }
            if(choice.equals("s")) {
                saveGame(totalPoints);
                System.out.println("Game saved");
                continue;
            }
            else if(choice.equals("l")) {
                totalPoints = loadGame();
                System.out.println("Loading save");
                continue;
            }
            else if(choice.equals("x")) {
                System.out.println("Exiting");
                System.exit(0);
            }
            else {
                boolean inputValid = false;
                while(!inputValid) {
                    try {
                        System.out.print("Please enter the X coordinate: ");
                        int fireX = Integer.parseInt(inputReader.readLine());
                        System.out.println();
                        System.out.print("Please enter the Y coordinate: ");
                        int fireY = Integer.parseInt(inputReader.readLine());
                        int gridCell = currentGrid[fireX][fireY];

                        if(gridCell != -1 && gridCell != 6) {
                            // If it's a ship
                            if(gridCell >= 1 && gridCell <= 5) {
                                ShipType type = shipIds.get(gridCell);
                                HashMap<Integer, Integer> shipCoords = ships.get(type).getCoords();
                                Set<Integer> xVals = shipCoords.keySet();
                                for(Integer x : xVals) {
                                    Integer y = shipCoords.get(x);
                                    grid.updateGrid(x.intValue(), y.intValue(), 6);
                                }

                                System.out.println(
                                        "You hit: " + type.name() + " for " + shipPoints.get(type) + " points!");
                                hits++;
                                totalPoints += shipPoints.get(type);
                                if(hits == maxHits) {
                                    gameOver = true;
                                    win = true;
                                }
                            }
                            // Else it's a miss
                            else {
                                grid.updateGrid(fireX, fireY, -1);
                                fails++;
                                System.out.println("Miss! " + (maxFails - fails) + " tries remaining");
                            }
                            System.out.println();
                            inputValid = true;
                        }
                        else {
                            System.out.println("You have already attacked that cell");
                        }
                    }
                    catch(NumberFormatException | IOException e) {
                        System.out.println("Invalid number");
                    }
                    catch(ArrayIndexOutOfBoundsException e) {
                        System.out.println("Coordinate out of bounds!");
                    }
                }
            }
        }
        if(win == false) {
            System.out.println("Game over! You scored " + totalPoints + " points!");
        }
        else {
            System.out.println("You win! You scored the maximum, " + totalPoints + " points!");
        }
    }

    public static void main(String[] args) {
        if(args.length > 0) {
            new Engine(true);
        }
        else {
            new Engine(false);
        }
    }

}
