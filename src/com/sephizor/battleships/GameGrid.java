package com.sephizor.battleships;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.Random;

import com.google.gson.Gson;

public class GameGrid {

    private int[][] grid;
    private Random rand;
    private final int GRID_SIZE = 10;

    public GameGrid() {
        grid = new int[GRID_SIZE][GRID_SIZE];
        rand = new Random();
    }

    public int[][] getGrid() {
        return grid;
    }

    public void updateGrid(int x, int y, int type) {
        grid[x][y] = type;
    }

    public void populate(Map<ShipType, Ship> ships) {
        for(ShipType type : ShipType.values()) {
            Ship ship = ships.get(type);
            boolean isVertical = rand.nextInt(1000) >= 500;
            boolean canFit = false;

            // check ship can fit
            while(!canFit) {
                int startPositionX = rand.nextInt(GRID_SIZE);
                int startPositionY = rand.nextInt(GRID_SIZE);
                canFit = setShipCells(isVertical, startPositionX, startPositionY, ship);
            }
        }
    }

    public void saveGame() {
        Gson gson = new Gson();
        String json = gson.toJson(this);
        File saveFile = new File(".save");
        BufferedWriter writer;
        try {
            writer = new BufferedWriter(new FileWriter(saveFile));
            writer.write(json);
            writer.close();
        }
        catch(IOException e) {
            System.err.println("Could not write save file");
        }
    }

    private boolean setShipCells(boolean isVertical, int startPositionX, int startPositionY, Ship ship) {
        int size = ship.getSize();
        if(!isVertical) {
            int endPosX = startPositionX + ship.getSize();
            if(endPosX > GRID_SIZE - 1) {
                // Grow ship to the left

                // for each space to the left up to max size, check it is empty
                for(int i = 0; i < size; i++) {
                    if(!isCellEmpty(startPositionX - i, startPositionY)) {
                        return false;
                    }
                }
                // Set the ship cells on the grid
                for(int i = 0; i < size; i++) {
                    grid[startPositionX - i][startPositionY] = ship.getId();
                    ship.setCoord(startPositionX - i, startPositionY);
                }
            }
            else {
                // Grow ship to the right

                for(int i = 0; i < size; i++) {
                    if(!isCellEmpty(startPositionX + i, startPositionY)) {
                        return false;
                    }
                }

                for(int i = 0; i < size; i++) {
                    grid[startPositionX + i][startPositionY] = ship.getId();
                    ship.setCoord(startPositionX + i, startPositionY);
                }
            }
        }
        else {
            int endPosY = startPositionY + ship.getSize();
            if(endPosY > GRID_SIZE - 1) {
                // Grow ship up

                for(int i = 0; i < size; i++) {
                    if(!isCellEmpty(startPositionX, startPositionY - i)) {
                        return false;
                    }
                }

                for(int i = 0; i < size; i++) {
                    grid[startPositionX][startPositionY - i] = ship.getId();
                    ship.setCoord(startPositionX, startPositionY - i);
                }
            }
            else {
                // Grow ship down

                for(int i = 0; i < size; i++) {
                    if(!isCellEmpty(startPositionX, startPositionY + i)) {
                        return false;
                    }
                }

                for(int i = 0; i < size; i++) {
                    grid[startPositionX][startPositionY + i] = ship.getId();
                    ship.setCoord(startPositionX, startPositionY + i);
                }
            }
        }
        return true;
    }

    private boolean isCellEmpty(int x, int y) {
        return grid[x][y] == 0;
    }
}
