package com.sephizor.battleships;

import java.util.HashMap;

public class Ship {
    private int _size;
    private int _id;
    private HashMap<Integer, Integer> coordinates;

    public Ship(int size, int id) {
        _size = size;
        _id = id;
        coordinates = new HashMap<Integer, Integer>();
    }

    public int getSize() {
        return _size;
    }

    public int getId() {
        return _id;
    }
    
    public void setCoord(int x, int y) {
        coordinates.put(x, y);
    }
    
    public HashMap<Integer, Integer> getCoords() {
        return coordinates;
    }
}
