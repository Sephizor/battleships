package com.sephizor.battleships;

public enum ShipType {
    AIRCRAFT_CARRIER, BATTLESHIP, SUBMARINE, DESTROYER, PATROL_BOAT
}
